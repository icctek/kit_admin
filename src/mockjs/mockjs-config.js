var APIs = {
  'POST /api/v1/user/login': {
    success: '@boolean',
    msg: '登录成功.',
    statusCode: 2000,
    token: 'e10adc3949ba59abbe56e057f20f883e'
  },
  'GET /api/v1/getUser': {
    id: 1,
    name: '@name',
    email: '@email',
    address: '@region',
    datetime: '@datetime'
  },
  'POST /api/v1/getmenus': [{
    id: 1,
    title: '首页',
    path: '#/',
    icon: '&#xe68e;',
    pid: 0,
    children: []
  }, {
    id: 2,
    title: '系统管理',
    icon: '&#xe60a;',
    open: true,
    pid: 0,
    children: [
      {
        id: 3,
        title: '资源管理',
        path: '#/res/list',
        icon: '&#xe60a;',
        pid: 2
      }, {
        id: 4,
        title: '角色管理',
        path: '#/role/list',
        icon: '&#xe613;',
        pid: 2
      }, {
        id: 5,
        title: '用户管理',
        path: '#/user/list',
        icon: '&#xe770;',
        pid: 2
      }
    ]
  }, {
    id: 6,
    title: '会员管理',
    path: '#/vip/list',
    icon: '&#xe60a;',
    open: true,
    pid: 0,
    children: [
    ]
  }]
};