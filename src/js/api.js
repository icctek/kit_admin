layui.define(function (exports) {
  var baseUrl = '\/\/127.0.0.1:8080/';  
  //var baseUrl = '/api/v1/';
  var api = {
    user: {
      login: baseUrl + 'login',
      getUsers: baseUrl + 'user/getusers',
      getMenus: baseUrl + 'user/getmenus',
      tableDate: baseUrl + 'user/data',
      save: baseUrl + 'user/save',
      opt: baseUrl + 'user/opt',
      remove: baseUrl + 'user/remove'
    }, 
    res: {
      remote: baseUrl + 'res/delete',
      treedata: baseUrl + 'res/data',
      selectTreeDate: baseUrl + 'res/selectTreeData',
      save: baseUrl + 'res/save',
      get: baseUrl + 'res/get',
      selectTreeDataThreeLevel: baseUrl + 'res/selectTreeDataThreeLevel'
    },
    role: {
      tableDate: baseUrl + 'role/data',
      save: baseUrl + 'role/save',
      get: baseUrl + 'role/get',
      opt: baseUrl + 'role/opt',
      remove: baseUrl + 'role/remove',
      selectResByRoleId: baseUrl + 'role/selectResByRoleId',
      selectAllRoles: baseUrl + 'role/selectAllRoles'
    },
    emp: {
      tableData: baseUrl + "emp/data"
    }
  };
  exports('api', api);
});